<?php

/**
 * @file
 * Provides a version of the combined field filter that works with Name fields.
 */

/**
 * Implements hook_views_data().
 */
function views_filter_combine_name_fields_views_data() {
  $data['views']['combine_namefields'] = array(
    'title' => t('Combine name fields filter'),
    'help' => t('Enhances the default Combine Fields Filter to work with Name fields'),
    'filter' => array(
    'help' => t('Enhances the default Combine Fields Filter to work with Name fields'),
      'handler' => 'views_filter_combine_name_fields_handler_filter_combine',
    ),
  );
  return $data;
}

